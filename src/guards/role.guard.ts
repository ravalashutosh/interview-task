import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ROLES_KEY } from './role.decorator'
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../user/user.schema';
import { Model } from 'mongoose';
import { ObjectId } from 'mongodb'
const jwt = require('jsonwebtoken');

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const requiredRoles = this.reflector.getAllAndOverride<any[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!requiredRoles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const auth = request.headers.authorization;
    if (auth) {
      const decodeToken = await jwt.verify(auth, "interview_practise")
      const user: any = await this.userModel.findOne({ _id: new ObjectId(decodeToken.id)}).populate("role")
      if (user) {
        Object.assign(request, { user })
        return requiredRoles.some((role) => user.role.name === role);
      }
      return false
    }
    return true
  }
}
