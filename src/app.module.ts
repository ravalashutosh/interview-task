import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema, User } from './user/user.schema'
import { RoleSchema, Role } from './user/role.schema'
import { AddressSchema, Address } from './user/address.schema'
import { RolesGuard } from './guards/role.guard'
import { APP_GUARD } from '@nestjs/core';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [
    MulterModule.register({
      dest: './files',
    }),
    MongooseModule.forRoot('mongodb://localhost/user-management'),
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Role.name, schema: RoleSchema },
      { name: Address.name, schema: AddressSchema },
    ])
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
  ],
})
export class AppModule {}
