
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './user/user.schema';
import { Role, RoleDocument } from './user/role.schema';
import { Address, AddressDocument } from './user/address.schema';

@Injectable()
export class AppService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
    @InjectModel(Role.name) private readonly roleModel: Model<RoleDocument>,
    @InjectModel(Address.name) private readonly addModel: Model<AddressDocument>,
  ) {}

  async create(createUserDto): Promise<User> {
    const createdUser = new this.userModel(createUserDto);
    return createdUser.save();
  }

  async createAddress(payload): Promise<Address> {
    const address = new this.addModel(payload);
    return address.save();
  }

  async insertRoles(payload): Promise<any> {
    return await this.roleModel.insertMany(payload, { rawResult: true });
  }

  async getRoles(query): Promise<any> {
    return await this.roleModel.find(query).lean().exec();
  }

  async findAll(query): Promise<User[]> {
    return await this.userModel.find(query).lean().exec();
  }

  async findOne(query): Promise<User> {
    return await this.userModel.findOne(query).lean().exec();
  }

  async update(query, payload): Promise<User> {
    const user =  await this.userModel.findOne(query);
    user.email = payload.email;
    user.name = payload.name;
    user.phone = payload.phone;
    user.image = payload.image;
    user.address = payload.address;
    return await user.save()
  }
}
