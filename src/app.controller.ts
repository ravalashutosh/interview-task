import { Controller, Get, Render, Post, BadRequestException, Body, Param, Req, Res, UseInterceptors, UploadedFile } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express'
import { AppService } from './app.service';
import { ObjectId } from 'mongodb'
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
import { Roles } from './guards/role.decorator'
import { Role } from './guards/role.enum'
import { diskStorage } from 'multer';
import {editFileName} from './helper/edit-file-name'

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('')
  @Roles([Role.User])
  @Render('user-profile')
  async userlist(@Req() req) {
    const user = req.user;
    return { user }
  }

  @Get('profile')
  @Roles([Role.User])
  async profile(@Req() req) {
    const user = req.user;
    return { user: {...user._doc, image: `files/${user.image}`} }
  }

  @Get('files/:imgpath')
  seeUploadedFile(@Param('imgpath') image, @Res() res) {
    return res.sendFile(image, { root: './files' });
  }

  @Get('user-login')
  @Render('user-login')
  async userLogin(@Req() req) {
    return ""
  }

  @Get('save-profile')
  @Roles([Role.User])
  @Render('save-profile')
  async profileSave(@Req() req) {
    return ""
  }

  @Post('save-profile')
  @Roles([Role.User])
  @UseInterceptors(FileInterceptor('file',
  {
    storage: diskStorage({
      destination: './files',
      filename: editFileName,
    })
  },
  ))
  async saveProfile(@Body() body, @UploadedFile() file, @Req() req) {
    const { user } = req;

    const address = await this.appService.createAddress({
      country: body.country,
      city: body.city,
      state: body.state,
      addressLine1: body.addressLine1,
      addressLine2: body.addressLine2,
      pin: body.pin,
    })

    const updatedUser =  await this.appService.update(
      {
        _id: new ObjectId(user._id)
      },
      {
        ...body,
        image: file ? file.filename : null,
        address: new ObjectId(address._id)
      }
    )
    return updatedUser;
  }

  @Get('create-user')
  @Render('create-user')
  async userCreate(@Req() req) {

    return ""
  }

  @Post('create-user')
  async createuser(@Body() body) {
    const roles = await this.appService.getRoles({})
    const userRole = roles.find(doc => doc.name === 'User')
    const password = await bcrypt.hash(body.password, 10)
    const user =  await this.appService.create({
      email:body.email,
      password,
      role: new ObjectId(userRole._id),
      name: body.name
    })
    return {
      access_token: jwt.sign({id: user._id }, 'interview_practise'),
      _id: user._id
    }
  }

  @Get('admin')
  @Roles([Role.Admin])
  @Render('list')
  async root() {
    const roles = await this.appService.getRoles({})
    const adminRole = roles.find(ele => ele.name === 'Admin');
    const query: any = { role: { $ne: new ObjectId(adminRole._id)}}
    const users = await this.appService.findAll(query)
    return { users };
  }

  @Get('admin-login')
  @Render('admin-login')
  async getadminLogin() {
    return ""
  }

  @Get('add-users')
  async addUsersAndRoles() {
    const roles = await this.appService.insertRoles([
      {
        name: "Admin"
      },
      {
        name: "User"
      }
    ])

    const adminRole = roles.ops.find(doc => doc.name === 'Admin')
    const password = await bcrypt.hash("Admin@123", 10)
    await this.appService.create({
      email: "admin@gmail.com",
      password,
      role: new ObjectId(adminRole._id),
      name: "Admin User"
    })
    return "admin user and roles are successfully added"
  }

  @Post('user-login')
  async loginUser(@Body() body) {
    try {
      const { email, password } = body;
      const roles = await this.appService.getRoles({})
      const userRole = roles.find(doc => doc.name === Role.User)
      const user = await this.appService.findOne({ email, role: new ObjectId(userRole._id) })
      console.log(user)
      if (!user) throw new Error('user does not exist')

      const matchPassword = await bcrypt.compare(password, user.password);
      if (!matchPassword) throw new Error('password does not match')

      return {
        access_token: jwt.sign({id: user._id }, 'interview_practise'),
        _id: user._id
      }
    } catch (err) {
      throw new BadRequestException('ADMIN_LOGIN.ERROR', (err as Error).message)
    }
  }

  @Post('admin-login')
  async adminLogin(@Body() body) {
    try {
      const { email, password } = body;
      const roles = await this.appService.getRoles({})
      const userRole = roles.find(doc => doc.name === Role.Admin)
      const user = await this.appService.findOne({ email, role: new ObjectId(userRole._id) })
      if (!user) throw new Error('user does not exist')

      const matchPassword = await bcrypt.compare(password, user.password);
      if (!matchPassword) throw new Error('password does not match')

      return {
        access_token: jwt.sign({id: user._id }, 'interview_practise'),
        _id: user._id
      }
    } catch (err) {
      throw new BadRequestException('ADMIN_LOGIN.ERROR', (err as Error).message)
    }
  }
}
