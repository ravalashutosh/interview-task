
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Role } from './role.schema'
import { ObjectId } from 'mongodb'

export type UserDocument = User & Document;

@Schema()
export class User {
  public _id: string;

  @Prop()
  name: string;

  @Prop()
  email: string;

  @Prop({ required: false })
  phone: string;

  @Prop()
  password: string;

  @Prop({ required: false })
  address: ObjectId;

  @Prop({ required: false })
  image: string;

  @Prop({ required: false, ref: "Role" })
  public role: ObjectId;
}

export const UserSchema = SchemaFactory.createForClass(User);
