
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Role } from './role.schema'
import { ObjectId } from 'mongodb'

export type AddressDocument = Address & Document;

@Schema()
export class Address {
  public _id: string;

  @Prop()
  addressLine1: string;

  @Prop()
  addressLine2: string;

  @Prop({ required: false })
  pin: string;

  @Prop({ required: true })
  city: string;

  @Prop({ required: true })
  state: string;

  @Prop({ required: true })
  country: string;
}

export const AddressSchema = SchemaFactory.createForClass(Address);
